$(function () {
  var status = $('#hire-us-status')
  var button = $('#submit-hire-us')
  var form = $('#hire-us-form').on('submit', function (event) {
    event.preventDefault()
    var data = form.serializeArray().reduce(function(data, field) {
      data[field.name] = field.value
      return data
    }, {})

    button.hide()
    status.text('Submitting...')

    $.ajax({
      url: form.attr('action'),
      method: "POST",
      data: data,
      dataType: "json"
    }).then(function () {
      form.find('.form-group').hide()
      status.text('Submitted. We will get back to you soon!')
    }).fail(function () {
      button.show()
      status.text('')
    })
  })
})
