$(function () {
  var $el = $('#js-site-nav')
  var $menu = $el.find('.site-nav-mobile')
  $el.on('click', '.site-nav-open, .site-nav-close', function (e) {
    $menu.toggleClass('open')
  })
  $el.on('click', function(e) { e.stopPropagation() })
  $(document.body).on('click', function () { $menu.removeClass('open') })
});